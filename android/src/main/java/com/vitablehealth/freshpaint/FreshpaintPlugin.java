package com.vitablehealth.freshpaint;

import androidx.annotation.NonNull;
import android.content.Context;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import com.freshpaint.android.Freshpaint;
import com.freshpaint.android.Properties;
import com.freshpaint.android.Traits;

import java.util.Map;

/** FreshpaintPlugin */
public class FreshpaintPlugin implements FlutterPlugin, MethodCallHandler {
  private MethodChannel channel;
  private Boolean isFreshpaintInited = false;
  private Context context;

  @Override
  public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
    channel = new MethodChannel(flutterPluginBinding.getBinaryMessenger(), "freshpaint");
    context = flutterPluginBinding.getApplicationContext();
    channel.setMethodCallHandler(this);
  }

  @Override
  public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
    if (call.method.equals("initFreshpaint")) {
      String envId = call.argument("envId");
      this.initFreshpaint(envId, result);
    }

    else if (call.method.equals("track")) {
      String eventName = call.argument("eventName");
      Map<String, Object> props = call.argument("properties");
      this.track(eventName, props);
    }

    else if (call.method.equals("identify")) {
      String userId = call.argument("userId");
      Map<String, Object> traits = call.argument("traits");
      this.identify(userId, traits);
    }

    else if (call.method.equals("screen")) {
      String screenName = call.argument("screenName");
      Map<String, Object> props = call.argument("properties");
      this.screen(screenName, props);
    }

    else if (call.method.equals("group")) {
      String groupId = call.argument("groupId");
      Map<String, Object> traits = call.argument("traits");
      this.group(groupId, traits);
    }
  }

  @Override
  public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
    channel.setMethodCallHandler(null);
  }

  private void initFreshpaint(String envId, MethodChannel.Result result) {
    if (isFreshpaintInited)
      return;

    Freshpaint freshpaint = new Freshpaint.Builder(this.context, envId)
            .trackApplicationLifecycleEvents()
            .build();

    Freshpaint.setSingletonInstance(freshpaint);
    isFreshpaintInited = true;
    result.success(true);
  }

  private void track(String eventName, Map<String, Object> properties) {
    Freshpaint
            .with(this.context)
            .track(eventName, getFreshpaintPropertiesFromMap(properties));
  }

  private void identify(String userId, Map<String, Object> traits) {
    Freshpaint
            .with(this.context)
            .identify(userId, getFreshpaintTraitsFromMap(traits), null);
  }

  private void screen(String screenName, Map<String, Object> properties) {
    Freshpaint
            .with(this.context)
            .screen(screenName, getFreshpaintPropertiesFromMap(properties));
  }

  private void group(String groupId, Map<String, Object> traits) {
    Freshpaint
            .with(this.context)
            .group(groupId, getFreshpaintTraitsFromMap(traits));
  }

  private Properties getFreshpaintPropertiesFromMap(Map<String, Object> map) {
    Properties properties = new Properties(map.size());

    for (String key : map.keySet()) {
      properties.putValue(key, map.get(key));
    }

    return properties;
  }

  private Traits getFreshpaintTraitsFromMap(Map<String, Object> map) {
    Traits traits = new Traits(map.size());

    for (String key : map.keySet()) {
      traits.putValue(key, map.get(key));
    }

    return traits;
  }
}
