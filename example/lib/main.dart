import 'package:flutter/material.dart';
import 'package:freshpaint_flutter/freshpaint.dart';


void main() async {
  runApp(const MyApp());
  await Freshpaint.init("YOUR_FP_TEST_KEY_HERE");
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Freshpaint Test',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Freshpaint Test Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  Future<void> _incrementCounter() async {
    // We use this method to test the Freshpaint SDK
    // You can test it by doing the following:
    //
    // 1. Uncomment one of the below Freshpaint calls
    // 2. Run the app on a simulator or physical device
    // 3. Press the increment button
    //
    // If the counter increments it most likely worked
    // i.e. no exceptions were raised.
    // In order to truly tell you need to go to Freshpaint
    // Live view and look at the Development project
    // to see if the events have come through properly.
    //
    // Unfortunately, we cannot unit test this code as we
    // need to ensure the call is making it to Freshpaint
    // and the native platform calls only work when the code
    // is being executed on a simulated or real machine
    // not in a unit test.

    // Track event

    Freshpaint.instance.track(
        eventName: "button-clicked",
        properties: {"name": "increment"}
    );


    // Identify event

    Freshpaint.instance.identify(
        userId: "vitable-user",
        traits: {
          "age": 25
        }
    );


    // Screen view event

    Freshpaint.instance.screen(
        screenName: "Home",
        properties: {"home_screen_property": "James Bond"}
    );


    // Group Event

    Freshpaint.instance.group(
        groupId: "Vitable",
        traits: {
          "company_id": "1337"
        }
    );

    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
