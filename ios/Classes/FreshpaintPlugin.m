#import "FreshpaintPlugin.h"
#if __has_include(<freshpaint_flutter/freshpaint_flutter-Swift.h>)
#import <freshpaint_flutter/freshpaint_flutter-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "freshpaint_flutter-Swift.h"
#endif

@implementation FreshpaintPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftFreshpaintPlugin registerWithRegistrar:registrar];
}
@end
