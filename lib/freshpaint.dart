
import 'dart:async';

import 'package:flutter/services.dart';

class Freshpaint {
  static const MethodChannel _channel = MethodChannel('freshpaint');
  static Freshpaint? _instance;

  static Freshpaint get instance {
    if (_instance == null) {
      throw Exception(
          "Freshpaint has not yet been initialized. Please call the "
              "'Freshpaint.init(envId: String)' method before "
              "attempting to access the Freshpaint instance."
      );
    }

    return _instance!;
  }

  Freshpaint._init();

  static Future<Freshpaint> init(String envId) async {
    if (_instance != null) {
      return _instance!;
    }

    await _channel.invokeMethod("initFreshpaint", <String, dynamic>{
      "envId": envId
    });

    _instance = Freshpaint._init();
    return _instance!;
  }

  void track({required String eventName, required Map<String, dynamic> properties}) {
    _channel.invokeMethod("track", <String, dynamic>{
      "eventName": eventName,
      "properties": properties
    });
  }

  void identify({required String userId, required Map<String, dynamic> traits}) {
    _channel.invokeMethod("identify", <String, dynamic>{
      "userId": userId,
      "traits": traits
    });
  }

  void screen({required String screenName, required Map<String, dynamic> properties}) {
    _channel.invokeMethod("screen", <String, dynamic>{
      "screenName": screenName,
      "properties": properties
    });
  }

  void group({required String groupId, required Map<String, dynamic> traits}) {
    _channel.invokeMethod("group", <String, dynamic>{
      "groupId": groupId,
      "traits": traits
    });
  }
}
