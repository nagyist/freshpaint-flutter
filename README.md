# Freshpaint for Flutter

<p align="center">
  <a href="https://gitlab.com/vitable/freshpaint-flutter/-/blob/main/LICENSE">
    <img alt="GitLab" src="https://img.shields.io/gitlab/license/32022810?color=sdf">
  </a>
</p>

## Getting Started

Call init before tracking any events, passing your Freshpaint environment id

```dart
import 'package:flutter/material.dart';
import 'package:freshpaint_flutter/freshpaint.dart';


void main() async {
  runApp(const MyApp());
  await Freshpaint.init("YOUR_FP_ENV_ID");
}
```

## All Freshpaint Events are Supported
```dart
// Track custom event
Freshpaint.instance.track(
    eventName: "button_clicked",
    properties: {"name": "increment"}
);

// Identify event
Freshpaint.instance.identify(
    userId: "vitable-user",
    traits: {
      "age": 25
    }
);

// Screen view event
Freshpaint.instance.screen(
    screenName: "Home",
    properties: {"home_screen_property": "James Bond"}
);

// Group Event
Freshpaint.instance.group(
    groupId: "Vitable",
    traits: {
      "company_id": "1337"
    }
);
```

## Installing
Add the following to your pubspec.yml
```yml
dependencies:
  freshpaint_flutter: ^0.0.1
```