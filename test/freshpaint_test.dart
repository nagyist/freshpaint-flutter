import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:freshpaint_flutter/freshpaint.dart';

void main() {
  const MethodChannel channel = MethodChannel('freshpaint');
  MethodCall? _methodCall;

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      _methodCall = methodCall;
      return true;
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('init', () async {
    Freshpaint.init("123");
    expect(
      _methodCall,
      isMethodCall(
        'initFreshpaint',
        arguments: <String, dynamic>{
          "envId": "123"
        },
      ),
    );
  });

  test('track', () async {
    await Freshpaint.init("123");
    Freshpaint.instance.track(
        eventName: "button-clicked",
        properties: {"name": "increment"}
    );

    expect(
      _methodCall,
      isMethodCall(
        'track',
        arguments: <String, dynamic>{
          "eventName": "button-clicked",
          "properties": {
            "name": "increment"
          }
        },
      ),
    );
  });

  test('identify', () async {
    await Freshpaint.init("123");
    Freshpaint.instance.identify(
        userId: "Batman",
        traits: {
          "age": 25
        }
    );

    expect(
      _methodCall,
      isMethodCall(
        'identify',
        arguments: <String, dynamic>{
          "userId": "Batman",
          "traits": {
            "age": 25
          }
        },
      ),
    );
  });

  test('screen', () async {
    await Freshpaint.init("123");
    Freshpaint.instance.screen(
        screenName: "Home",
        properties: {"home-screen-property": "James Bond"}
    );

    expect(
      _methodCall,
      isMethodCall(
        'screen',
        arguments: <String, dynamic>{
          "screenName": "Home",
          "properties": {
            "home-screen-property": "James Bond"
          }
        },
      ),
    );
  });

  test('group', () async {
    await Freshpaint.init("123");
    Freshpaint.instance.group(
        groupId: "Vitable",
        traits: {
          "company_id": "1337"
        }
    );

    expect(
      _methodCall,
      isMethodCall(
        'group',
        arguments: <String, dynamic>{
          "groupId": "Vitable",
          "traits": {
            "company_id": "1337"
          }
        },
      ),
    );
  });
}
