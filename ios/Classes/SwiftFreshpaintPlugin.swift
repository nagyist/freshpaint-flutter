import Flutter
import UIKit
import FreshpaintSDK

public class SwiftFreshpaintPlugin: NSObject, FlutterPlugin {
  var isFreshpaintInited: Bool = false
  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "freshpaint", binaryMessenger: registrar.messenger())
    let instance = SwiftFreshpaintPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    if call.method == "initFreshpaint" {
        let args = call.arguments as! Dictionary<String, Any>
        self.initFreshpaint(
            envId: args["envId"] as! String,
            result: result
        )
    }

    else if call.method == "track" {
        let args = call.arguments as! Dictionary<String, Any>
        self.track(
            eventName: args["eventName"] as! String,
            properties: args["properties"] as! Dictionary<String, Any>
        )
    }

    else if call.method == "identify" {
        let args = call.arguments as! Dictionary<String, Any>
        self.identify(
            userId: args["userId"] as! String,
            traits: args["traits"] as! Dictionary<String, Any>
        )
    }

    else if call.method == "screen" {
        let args = call.arguments as! Dictionary<String, Any>
        self.screen(
            screenName: args["screenName"] as! String,
            properties: args["properties"] as! Dictionary<String, Any>
        )
    }

    else if call.method == "group" {
        let args = call.arguments as! Dictionary<String, Any>
        self.group(
            groupId: args["groupId"] as! String,
            traits: args["traits"] as! Dictionary<String, Any>
        )
    }
  }

  private func initFreshpaint(envId: String, result: FlutterResult) {
      if isFreshpaintInited {
          return
      }

      let config = FreshpaintConfiguration(writeKey: envId)
      config.trackApplicationLifecycleEvents = true
      Freshpaint.setup(with: config)
      isFreshpaintInited = true

      result(true)
  }


  private func track(eventName: String, properties: Dictionary<String, Any>) {
      Freshpaint.shared().track(
          eventName,
          properties: properties
      )
  }

  private func identify(userId: String, traits: Dictionary<String, Any>) {
      Freshpaint.shared().identify(
          userId,
          traits: traits
      )
  }

  private func screen(screenName: String, properties: Dictionary<String, Any>) {
      Freshpaint.shared().screen(
          screenName,
          properties: properties
      )
  }

  private func group(groupId: String, traits: Dictionary<String, Any>?) {
      Freshpaint.shared().group(
          groupId,
          traits: traits
      )
  }
}
